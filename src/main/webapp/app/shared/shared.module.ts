import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestConnexionSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [TestConnexionSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [TestConnexionSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestConnexionSharedModule {
  static forRoot() {
    return {
      ngModule: TestConnexionSharedModule
    };
  }
}
