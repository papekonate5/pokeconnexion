import { NgModule } from '@angular/core';

import { TestConnexionSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [TestConnexionSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [TestConnexionSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TestConnexionSharedCommonModule {}
