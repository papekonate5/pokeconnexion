package com.sogui.test.service;


import com.sogui.test.config.ApplicationProperties;
import com.sogui.test.domain.CodeOTPInfos;
import org.jsmpp.bean.*;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SMSNotificationService {

    private final Logger log = LoggerFactory.getLogger ( SMSNotificationService.class );

    private final ApplicationProperties applicationProperties;

    public SMSNotificationService(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public boolean messageNotify(CodeOTPInfos infos) {

        return refactorNotify(infos);
    }

    private boolean refactorNotify(CodeOTPInfos infos){

        String formaMessage = applicationProperties.getSmsText().replace("$",infos.getCode ());
        return sendSMSPP(infos.getMsisdn(),formaMessage);

    }

    public boolean sendSMSPP(String msisdn, String message) {

        log.trace("send smspp {} {}", msisdn, message);

        try {

            SMPPSession session = new SMPPSession();

            session.connectAndBind(applicationProperties.getSmsHost(), applicationProperties.getSmsPort(),

                new BindParameter(

                    BindType.BIND_TX, applicationProperties.getSmsSystemId(), applicationProperties.getSmsPassword(),

                    applicationProperties.getSmsSystemType(), TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, null));

            session.submitShortMessage("CMT", TypeOfNumber.ALPHANUMERIC, NumberingPlanIndicator.UNKNOWN,

                "OrangeEtMoi", TypeOfNumber.NATIONAL, NumberingPlanIndicator.UNKNOWN,

                msisdn, new ESMClass(), (byte) 0, (byte) 1, null, null,

                new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT), (byte) 0, new GeneralDataCoding(Alphabet.ALPHA_DEFAULT, MessageClass.CLASS1, false),

                (byte) 0, message.getBytes());

            session.unbindAndClose();

            log.info("SMS sent successfully!");
        }

        catch (Exception ex){

            log.error("Exception during sendSMS: {} ", ex);

        }

        return true;

    }


}
