package com.sogui.test.service.util;

import java.util.Random;

/**
 * Created by centonni on 15/11/18.
 */
public class CodeOTPUtil {

    private CodeOTPUtil() {
    }

    public static String generateCodeOTP() {

        return (new Random().nextInt((999999 - 100000) + 1) + 100000) + "";
    }

}
