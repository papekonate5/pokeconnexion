package com.sogui.test.service.util;

public final class Message {

    private Message() {
        super ();
    }


    public static final class CodeOTP {

        public static final String CREATION = "Creation du code OTP";
        public static final String VERIF = "Verification du code OTP";


        private CodeOTP() {
            super ();
        }
    }

    public static final class Register {

        public static final String VERIF = "Verification de la requete d enregistrement abonne";


        private Register() {
            super ();
        }
    }

    public static final class MessageService{

        public static final String SEND_MESSAGE = "Envoie d un message par sms";

        public MessageService() {
            //Default constructor
        }
    }


}
