package com.sogui.test.service.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormatPhoneNumberUtil {

    private static final Pattern EXTRACT_MOBILE_NUMBER = Pattern.compile(Constants.LOGIN_REGEX_VALID_NUMBER);

    private FormatPhoneNumberUtil() {
        // default constructor
    }

    public static String extractNumberWithoutSuffix(String msisdn) {

        Matcher matcher = EXTRACT_MOBILE_NUMBER.matcher(msisdn);

        if (matcher.matches()) {
            StringBuilder numberbuilder = new StringBuilder();

            numberbuilder.append(matcher.group(2))

                .append(matcher.group(3))

                .append(matcher.group(4))

                .append(matcher.group(5));

            return numberbuilder.toString();
        }

        return "";

    }

}
