package com.sogui.test.service.impl;

import com.sogui.test.domain.CodeOTPInfos;
import com.sogui.test.repository.CodeOTPInfosRepository;
import com.sogui.test.service.CodeOTPInfosService;
import com.sogui.test.service.util.CodeOTPUtil;
import com.sogui.test.service.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing CodeOTPInfos.
 */
@Service
@Transactional
public class CodeOTPInfosServiceImpl implements CodeOTPInfosService {

    private final Logger log = LoggerFactory.getLogger ( CodeOTPInfosServiceImpl.class );

    private final CodeOTPInfosRepository codeOTPInfosRepository;

    private final Environment environment;

    public CodeOTPInfosServiceImpl(CodeOTPInfosRepository codeOTPInfosRepository, Environment environment) {
        this.codeOTPInfosRepository = codeOTPInfosRepository;
        this.environment = environment;
    }

    /**
     * Save a codeOTPInfos.
     *
     * @param codeOTPInfos the entity to save
     * @return the persisted entity
     */
    @Override
    public CodeOTPInfos save(CodeOTPInfos codeOTPInfos) {
        log.debug ( "Request to save CodeOTPInfos : {}", codeOTPInfos );
        return codeOTPInfosRepository.save ( codeOTPInfos );
    }

    @Override
    public boolean checkCodeOTP(String msisdn, String codeotp) {
        log.debug ( "check code otp fo  msisdn {}", msisdn );
        Optional<CodeOTPInfos> retrieved = codeOTPInfosRepository
            .findTopByMsisdnAndCodeOrderByDateDesc ( msisdn, codeotp );
        log.debug ( "otpinfos {}", retrieved );

        boolean check=false;

        if (retrieved.isPresent()){

            CodeOTPInfos otpInfos = retrieved.get();

            check= Duration.between (otpInfos.getDate (), ZonedDateTime.now () )
                .getSeconds () < Constants.MAX_DELAY_TO_TRY;

            otpInfos.setVerified(check);
            codeOTPInfosRepository.save(otpInfos);
        }

        return check ;
    }

    @Override
    public boolean checkRegisterRequest(String msisdn) {
        Optional<CodeOTPInfos> otpInfos = codeOTPInfosRepository.findTopByMsisdnAndCaptchaTokenNotNullAndAndVerifiedTrue(msisdn);
        return otpInfos.isPresent();
    }

    @Override
    public String generateCode() {
        for (final String profileName : environment.getActiveProfiles()) {
            if (profileName.equals("test")){
                return "654321";
            }
        }
        return CodeOTPUtil.generateCodeOTP ();
    }

}
