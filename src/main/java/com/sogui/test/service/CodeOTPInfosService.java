package com.sogui.test.service;

import com.sogui.test.domain.CodeOTPInfos;

/**
 * Service Interface for managing CodeOTPInfos.
 */
public interface CodeOTPInfosService {

    /**
     * Save a codeOTPInfos.
     *
     * @param codeOTPInfos the entity to save
     * @return the persisted entity
     */
    CodeOTPInfos save(CodeOTPInfos codeOTPInfos);


    boolean checkCodeOTP(String msisdn, String code);

    boolean checkRegisterRequest(String msisdn);

    String generateCode();

}
