package com.sogui.test.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Selfcare Otp.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private String smsHost;
    private int smsPort;
    private String smsSystemId;
    private String smsSystemType;
    private String smsPassword;
    private String smsText;

    public String getSmsHost() {
        return smsHost;
    }

    public void setSmsHost(String smsHost) {
        this.smsHost = smsHost;
    }

    public int getSmsPort() {
        return smsPort;
    }

    public void setSmsPort(int smsPort) {
        this.smsPort = smsPort;
    }

    public String getSmsSystemId() {
        return smsSystemId;
    }

    public void setSmsSystemId(String smsSystemId) {
        this.smsSystemId = smsSystemId;
    }

    public String getSmsSystemType() {
        return smsSystemType;
    }

    public void setSmsSystemType(String smsSystemType) {
        this.smsSystemType = smsSystemType;
    }

    public String getSmsPassword() {
        return smsPassword;
    }

    public void setSmsPassword(String smsPassword) {
        this.smsPassword = smsPassword;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }
}
