package com.sogui.test.web.rest;

import com.sogui.test.domain.CodeOTPInfos;
import com.sogui.test.service.CaptchaService;
import com.sogui.test.service.CodeOTPInfosService;
import com.sogui.test.service.SMSNotificationService;
import com.sogui.test.service.util.Constants;
import com.sogui.test.service.util.FormatPhoneNumberUtil;
import com.sogui.test.web.rest.vm.CodeOTPCheckVM;
import com.sogui.test.web.rest.vm.GenererCodeOTPVM;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * REST controller for managing CodeOTPInfos.
 */
@RestController
@RequestMapping("/api")
public class CodeOTPInfosResource {


     private  final CaptchaService captchaService;

    private final CodeOTPInfosService codeOTPInfosService;

    private final SMSNotificationService notificationService;

    public CodeOTPInfosResource(CodeOTPInfosService codeOTPInfosService, SMSNotificationService notificationService,CaptchaService captchaService) {
        this.codeOTPInfosService = codeOTPInfosService;
        this.notificationService = notificationService;
        this.captchaService = captchaService;
    }

    @PostMapping("/code-otp-infos/generate")
    public ResponseEntity<GenererCodeOTPVM> generateCodeOTP(
        @Valid @RequestBody GenererCodeOTPVM otpvm) {

        otpvm.setMsisdn ( FormatPhoneNumberUtil.extractNumberWithoutSuffix( otpvm.getMsisdn () ) );

        CodeOTPInfos otpInfos = new CodeOTPInfos ();
        otpInfos.setMsisdn ( FormatPhoneNumberUtil.extractNumberWithoutSuffix( otpvm.getMsisdn () ) );

        String code = codeOTPInfosService.generateCode();
        otpInfos.setCode ( code );
        otpInfos.setDate ( ZonedDateTime.now () );
        otpInfos.setMsisdn (
            FormatPhoneNumberUtil.extractNumberWithoutSuffix( otpInfos.getMsisdn () ) );
        //otpInfos.setCaptchaToken(otpvm.getToken());
        codeOTPInfosService.save ( otpInfos );
        notificationService.messageNotify ( otpInfos );
        otpvm.setOtpMessageSent ( true );
         otpvm.setDuration ( Math.toIntExact ( Constants.MAX_DELAY_TO_TRY ) );


        return ResponseEntity.ok ().body ( otpvm );

    }



    @PostMapping("/code-otp-infos/check")
    public ResponseEntity<CodeOTPCheckVM> registerOTP(
        @Valid @RequestBody CodeOTPCheckVM checkVM) {

        checkVM.setMsisdn ( FormatPhoneNumberUtil.extractNumberWithoutSuffix( checkVM.getMsisdn () ) );
        boolean checkCodeOTP = codeOTPInfosService.checkCodeOTP ( checkVM.getMsisdn (),
            checkVM.getCode () );

        checkVM.setValid ( checkCodeOTP );

        return ResponseEntity.ok ( checkVM );
    }

    @GetMapping("/code-otp-infos/check-valid-request/{msisdn}")
    public ResponseEntity<Map<String, Boolean>> registerCheckValidRequest(@PathVariable String msisdn) {

        Map<String,Boolean> response = new HashMap<>();
        response.put("valid",codeOTPInfosService.checkRegisterRequest(msisdn));

        return ResponseEntity.ok(response);
    }


}
