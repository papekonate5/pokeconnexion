/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sogui.test.web.rest.vm;
