package com.sogui.test.web.rest;

import com.sogui.test.service.SMSNotificationService;
import com.sogui.test.web.rest.vm.MessageVM;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;

/**
 * REST controller for managing CodeOTPInfos.
 */
@RestController
@RequestMapping("/api/message")
public class MessageResource {

    private final SMSNotificationService messageService;

    public MessageResource(SMSNotificationService messageService) {
        this.messageService = messageService;
    }


    @PostMapping("/send")
    public boolean generateMessage(@Valid @RequestBody MessageVM messageVM) {

        return messageService.sendSMSPP(messageVM.getMsisdn(),messageVM.getMessage());

    }
}
