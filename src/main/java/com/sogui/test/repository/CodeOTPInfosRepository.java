package com.sogui.test.repository;

import com.sogui.test.domain.CodeOTPInfos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the CodeOTPInfos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CodeOTPInfosRepository extends JpaRepository<CodeOTPInfos, Long> {

    Optional<CodeOTPInfos> findTopByMsisdnAndCodeOrderByDateDesc(String msisdn,
                                                                 String code);
    Optional<CodeOTPInfos> findTopByMsisdnAndCodeOrderByDateAsc(String msisdn,
                                                                String code);

    Optional<CodeOTPInfos> findTopByMsisdnAndCaptchaTokenNotNullAndAndVerifiedTrue(String msisdn);


}
